#include "SoundManager.h"
#include <iostream>  
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>


using namespace std;


SoundManager::SoundManager()
{

}

SoundManager::~SoundManager(){};


void SoundManager::setBackgroundSong(LPCSTR filename)
{
	PlaySound(filename, NULL, SND_FILENAME | SND_LOOP | SND_ASYNC);
}

void SoundManager::playSound(LPCSTR filename)
{
	PlaySound(filename, NULL, SND_FILENAME | SND_ASYNC);
}

//Call two above methods with something such as: sound.setBackgroundSong((LPCSTR)"C:\\song.wav");

void SoundManager::stopSound()
{
	PlaySound(NULL, 0, 0);
}

void SoundManager::addSongToQueue(LPCSTR filename)
{
	soundsQueue.push(filename);	
}

void SoundManager::playSongFromQueue()
{
	if (!soundsQueue.empty())
	{
		playSound(soundsQueue.front());
		soundsQueue.pop();
	}
}


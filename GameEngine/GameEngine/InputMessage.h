#pragma once
#include "Message.h"

class InputMessage : public Message
{
	void handle(Controller controller1, const std::string& message);
};

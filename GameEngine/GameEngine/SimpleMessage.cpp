#include "SimpleMessage.h"

SimpleMessage::SimpleMessage(const std::string& Entity, const std::string& mMessage)
{
	this->Message = mMessage;
	this->Entity = Entity;
}

SimpleMessage::SimpleMessage(const std::string& Entity, int x, int y)
{
	this->Entity = Entity;
	this->x = x;
	this->y = y;
}

SimpleMessage::SimpleMessage(const std::string& Entity, int x)
{
	this->Entity = Entity;
	this->x = x;
}

std::string SimpleMessage::handle(IMessageHandler* Handler)
{
	return Handler->process(this);
}

std::string SimpleMessage::getMessage()
{
	return this->Message;
}
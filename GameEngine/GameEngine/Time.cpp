#include "Time.h"
#include <Windows.h>


Time::Time()
{
	frameStartTime = GetTickCount();
	if (previousTime == 0)
	{
		previousTime = frameStartTime;
	}

	timeDifference = (frameStartTime - previousTime) / 1000.0f;

}


Time::~Time()
{
}

void Time::update()
{
	//// all the time stuff
	frameStartTime = GetTickCount();
	if (previousTime == 0)
	{
		previousTime = frameStartTime;
	}

	timeDifference = (frameStartTime - previousTime) / 1000.0f;
}

float Time::getTime()
{
	return timeDifference;
}

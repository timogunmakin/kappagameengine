#pragma once
#include <string>
#include "MeshLoader.h"
#include "ICamera.h"
/*This is the Graphics Interface which handles Graphics rendering*/
class IGraphics
{
private:

protected:

public:
	
	virtual void poll(ICamera* camera) = 0;
	
	/*This method will draw the window of the application using the desired library, if initializing fails then return false*/
	virtual bool initialize() = 0;
	
	/*The draw method will also be overridden depending on the rendering choice, OPENGL or DirectX or any other library, start draw handles setting up the drawing*/
	virtual void startDraw() = 0;
	/*End draw prepares to swap buffers*/
	virtual void endDraw() = 0;

	//virtual void draw(float3 scale, float3, rotate, float3 translate);
	virtual void draw(/*ModelLoader *mesh*/) = 0;

//	virtual void draw(ModelLoader *mesh, ) = 0;

	

};


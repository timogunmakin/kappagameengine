#include "RendererComponent.h"
#include <iostream>


RendererComponent::RendererComponent()
{
	/*creates scene graph for all game objects in game*/
	
		
		//sceneGraph->push_back(*cube);
	std::cout << "Adding cube to Scene graph.." << std::endl;
	
	
	//this->draw();
	
	std::cout << "Creating Scene graph..." << std::endl;
	
	//	cube->addComponent(*graphics);
	//	sceneGraph->push_back(*cube);
}

void RendererComponent::SetRenderLib(IGraphics* target, int argc, char**argv)
{
	graphics = target;
}
void RendererComponent::poll(ICamera* camera)
{
	graphics->poll(camera);
}
bool RendererComponent::initialize()
{
	return graphics->initialize();
}

void RendererComponent::addGameObject(std::string objectName, std::string objectModelFile, std::string objectTextureFile)
{
	sceneGraph = SceneGraphSingleton::getInstance();
	/*adds a new gameobject to the list called cube*/
	GameObject *cube = new GameObject(objectName, objectModelFile, objectTextureFile);

	/*addComponent can be improved by using a message system. better way would be to input string instead of an object*/
	sceneGraph->populateSceneGraph(cube);
}

void RendererComponent::update()
{
	sceneGraph = SceneGraphSingleton::getInstance();

	for (int i = 0; i < sceneGraph->getMeshCount(); i++)
	{
		sceneGraph->getGameObject(i)->update();
	}

}

void RendererComponent::draw()
{
	
	graphics->startDraw();
	graphics->draw();
	graphics->endDraw();
	
	//graphics->initialize();
	
}

RendererComponent::~RendererComponent()
{
}

GameObject* RendererComponent::getGameObject(std::string playerTarget)
{
	return SceneGraphSingleton::getInstance()->getGameObject(playerTarget);
}
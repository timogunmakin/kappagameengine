#include "Physics.h"

Physics::Physics()
{
	pos.x = 0;
	pos.y = 0;
	pos.z = 0;
	prevPos.x = 0;
	prevPos.y = 0;
	prevPos.z = 0;
	displacement.x = 0; 
	displacement.y = 0;
	displacement.z = 0;
	velocity.x = 0.0;
	velocity.y = 0.0;
	velocity.z = 0.0;
	netForce.x = 0;
	netForce.y = 0;
	netForce.z = 0;
	friction.x = 0;
	friction.y = 0;
	friction.z = 0;
	mass = 0;
	acceleration.x = 0;
	acceleration.y = 0;
	acceleration.z = 0;
	drag = 1.05f; 
}

Physics::~Physics()
{
}

//Position
void Physics::setPosition(float xPos, float yPos, float zPos)
{
	pos.x = xPos;
	pos.y = yPos;
	pos.z = zPos;
}
Point3D Physics::getPosition()
{
	return pos;
}
void Physics::setX(float x)
{
	pos.x = x;
}
void Physics::setY(float y)
{
	pos.y = y;
}
void Physics::setZ(float z)
{
	pos.z = z;
}
float Physics::getX()
{
	return pos.x;
}
float Physics::getY()
{
	return pos.y;
}
float Physics::getZ()
{
	return pos.z;
}
Point3D Physics::getPrevPosition()
{
	return prevPos;
}

//Displacement
Point3D Physics::getDisplacement()
{
	return displacement;
}

//Velocity
Point3D Physics::getVelocity()
{
	return velocity;
}
void Physics::setVelocity(float x, float y)
{
	velocity.x = x;
	velocity.y = y;
}

//NetForce
Point3D Physics::getNetForce()
{
	return netForce;
}
void Physics::setNetForce(float forcea, float forceb)
{
	netForce.x = forcea;
	netForce.y = forceb;
}
void Physics::updateNetForce(float x1, float y1, float z1, float x2, float y2, float z2)
{
	netForce.x = x1 + x2;
	netForce.y = y1 + y2;
	netForce.z = z1 + z2;
}

//Acceleration
Point3D Physics::getAcceleration()
{
	return acceleration;
}
void Physics::updateAcceleration()
{
	acceleration.x = netForce.x / mass;
	acceleration.y = netForce.y / mass;
	acceleration.z = netForce.z / mass;
}

//Mass
void Physics::setMass(float m)
{
	mass = m;
}
float Physics::getMass()
{
	return mass;
}

//Drag
float Physics::getDragCoef()
{
	return drag;
}


//Collisiion Detection Check - No action done from this
bool Physics::collisionDetection(const Point3D& o)
{
	return (abs(pos.x - o.x) * 2 < (10 + 10)) && (abs(pos.y - o.y) * 2 < (10 + 10)) && (abs(pos.z - o.z) * 2 < (10 + 10));
}
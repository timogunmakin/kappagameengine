#pragma once
#include "IMessage.h"

class SimpleMessage : public IMessage
{
private:
	std::string Entity;
	std::string Message;
	int x;
	int y;
public:
	SimpleMessage(const std::string& entity, const std::string& message);
	SimpleMessage(const std::string& entity, int x, int y);
	SimpleMessage(const std::string& entity, int x);
	std::string handle(IMessageHandler* MessageHandler) override;
	std::string getMessage() override;
	      
};

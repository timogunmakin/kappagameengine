#pragma once
#include "IComponent.h"
#include "IGraphics.h"
#include "GameObject.h"
#include "OpenGL.h"
#include <string>
#include <vector>
class RendererComponent : public IComponent
{
private:
	IGraphics *graphics;
	SceneGraphSingleton *sceneGraph;

protected:

public:
	RendererComponent();
	~RendererComponent();
	
	bool initialize() override;
	
	void update() override;
	void poll(ICamera* camera);
	void draw();
	void addGameObject(std::string objectName, std::string objectModelFile, std::string objectTextureFile);
	void SetRenderLib(IGraphics* target, int argc, char**argv);
	GameObject* getGameObject(std::string playerTarget);
};


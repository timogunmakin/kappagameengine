#pragma once
//#include "IFSMPatrol.h"
#include "IFSMRun.h"
#include "IFSMSteering.h"
#include "IFSMWalk.h"
#include "IFSMStand.h"
#include "SimpleMessage.h"
class FSMGuard : public IFSMRun , IFSMWalk , IFSMStand , IFSMSteering
{
private:
	int walkSpeed;
	int runSpeed;
	int standDuration;
	int moveDuration;
	SimpleMessage* inputMessage;
public:
	void Initialize();
	void steeringAmount() override;
	void Run() override;
	void Walk() override;
	void Stand() override;
	int setWalkSpeed(int walkSpeed) override;
	int setRunSpeed(int runSpeed) override;
	int setStandDuration(int standDuration) override;
	int setMoveDuration(int MoveDuration);
	void FSMLogic();


};
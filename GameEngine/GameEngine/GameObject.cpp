#include "GameObject.h"
#include "RendererComponent.h"


GameObject::GameObject(const std::string &newName, const std::string &meshPath, const std::string &texturePath)
: name(newName)
{
	components.push_back(&transform);
	
	Vector3 defaultPosition;
	defaultPosition.x = 0.0f;
	defaultPosition.y = 0.0f;
	defaultPosition.z = 0.0f;

	transform.setPosition(defaultPosition);
	mesh = new MeshLoader(meshPath, texturePath, 0);
	mesh->read_file(meshPath);
	mesh->loadTexture(texturePath);
	

}

GameObject::GameObject()
{

}

GameObject::~GameObject()
{

}

Vector3 GameObject::getPosition()
{
	return transform.getPosition();
}

void GameObject::setPosition(Vector3 newPosition)
{
	transform.setPosition(newPosition);
}

Vector3 GameObject::getScale()
{
	return transform.getScale();
}

void GameObject::setScale(Vector3 newScale)
{
	transform.setScale(newScale);
}

void GameObject::addComponent(IComponent& newComponent)
{
	//find component by string
	components.push_back(&newComponent); 

}




void GameObject::assignComponentUnderComponent(IComponent* newComponent, IComponent* targetComponent)
{
	targetComponent->assignComponent(*newComponent);
}

void GameObject::update()
{
	for (int i = 0; i < components.size(); i++)
	{
		components[i]->update();
	}
	
}



const std::string& GameObject::getName() const
{
	return name;
}

void GameObject::setPlayer()
{
	isPlayer = true;
}

bool GameObject::getIsPlayer()
{
	return isPlayer;
}

int GameObject::getPlayerNum()
{
	return playerNum;
}

IComponent* GameObject::getComponent(IComponent* targetComponent)
{
	for (int i = 0; i < components.size(); i++)
	{
		if (components[i]->getComponentName() == targetComponent->getComponentName())
		{
			return components[i];
		}
	}

	return nullptr;
	
}
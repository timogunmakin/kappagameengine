#pragma once
#include "VectorData.h"
#include "IProperty.h"

class Acceleration : public IProperty
{
private:
	Vector3	acceleration;

public:
	Acceleration();
	~Acceleration();
	void update() override;
	void calculateAcceleration();
};


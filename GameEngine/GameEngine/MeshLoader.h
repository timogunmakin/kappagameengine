#pragma once

#include <string>
class MeshLoader
{

private:
	typedef struct
	{
		float x;
		float y;
		float z;
	} circle;
	typedef struct
	{
		float a;
		float b;
		float c;
		float d;
	} polygon;
	
	std::string fileName;
	std::string texture;
	circle * vertices;
	polygon * indices;
	int NUM_VERTS;
	int NUM_POLY;
protected: 
	int Model_Texture_Num;
	float* normals;							// Stores the normals
	float* Faces_Triangles;					// Stores the triangles
	float* vertexBuffer;					// Stores the points which make the object
	long TotalConnectedPoints;				// Stores the total number of connected verteces
	long TotalConnectedTriangles;			// Stores the total number of connected triangles

public:
	MeshLoader(std::string Model_fileName, std::string Model_texture, int ModlTextNum)
		: fileName(Model_fileName), texture(Model_texture), Model_Texture_Num(ModlTextNum)
	{
		
	}
	~MeshLoader();
	float* calculateNormal(float *coord1, float *coord2, float *coord3);
	float* getNormals();
	float* getFaces(); 
	long getTotalConnectedTriangles();
	int read_file(const std::string fileName);
	int loadTexture(const std::string fileName);
	void drawPolygon(int pointA, int pointB, int pointC);
	void drawObject();
	MeshLoader getMesh();
	int getNumPoly() { return NUM_POLY; }
	int getTextureNum() { return Model_Texture_Num; }
	circle* getVertices() { return vertices; }
	polygon* getIndices() { return indices; }
};



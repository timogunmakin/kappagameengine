#include "PlayerComponent.h"



PlayerComponent::PlayerComponent()
{
}


PlayerComponent::~PlayerComponent()
{
}


GameObject* PlayerComponent::getPlayer(int playerNum)
{
	return SceneGraphSingleton::getInstance()->getPlayerGameObject(playerNum);
}

GameObject* PlayerComponent::getPlayer(std::string player)
{
	return SceneGraphSingleton::getInstance()->getGameObject(player);
}



#pragma once
class IFSMRun
{
public:
	virtual void Run() = 0;
	virtual int setRunSpeed(int speed) = 0;
};

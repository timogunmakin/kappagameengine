#pragma once
#include <vector>
#include "IComponent.h"
#include "GameObject.h"
#include "SceneGraphSingleton.h"
class PlayerComponent : public IComponent
{
private:
	SceneGraphSingleton* sceneGraph;

public:
	PlayerComponent();
	~PlayerComponent();

	bool initialize() override { return false;  };
	
	void update() override {};
	void addNewPlayer();
	GameObject* getPlayer(int playerNum);
	GameObject* getPlayer(std::string playerNum);

};


#include "FSMGuard.h"

void FSMGuard::Initialize()
{
/*	switch (inputMessage->getMessage())
	setWalkSpeed();
	setRunSpeed();
	setStandDuration();
	*/
}



void FSMGuard::FSMLogic()
{
	if (standDuration >= 0)
	{
		Initialize();
	}
	if (inputMessage->getMessage() == "Walk")
	{
		for (int i = 0; i < moveDuration; i++)
		{
			Walk();
		}
	}
	else if (inputMessage->getMessage() == "Run")
	{
		for (int i = 0; i < moveDuration; i++)
		{
			Run();
		}
	}
	else for (int i = 0; i < standDuration; i++)
	{
		Stand();
	}
}

int FSMGuard::setWalkSpeed(int wSpeed)
{
	walkSpeed = wSpeed;
	return walkSpeed;
}
//Sets run speed for object
int FSMGuard::setRunSpeed(int rSpeed)
{
	runSpeed = rSpeed;
	return runSpeed;
}
//Sets pause between each movement for object, 
// i.e. Metal Gear Solid Soldiers - short pause before turning.
int FSMGuard::setStandDuration(int pause)
{
	standDuration = pause;
	return standDuration;
}
//Updates object position each frame
void FSMGuard::Walk()
{
	//	Each frame increments movement (60fps default) - "object" placeholder variable name
//	object.pos += (walkSpeed / 60); 
	inputMessage = new SimpleMessage("Forwards", moveDuration, walkSpeed);
}

void FSMGuard::Run()
{
	//	Each frame increments movement (60fps default) - "object" placeholder variable name
//	object.pos += (runSpeed / 60); 
	inputMessage = new SimpleMessage("Forwards", moveDuration, runSpeed);
}

void FSMGuard::Stand()
{
	//	Each frame increments movement (60fps default) - "object" placeholder variable name
	//	object.pos += (runSpeed / 60); 
	inputMessage = new SimpleMessage("Nothing", moveDuration);
}

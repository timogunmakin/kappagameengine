#pragma once
#include "GameObject.h"
#include "VectorData.h"

class ICamera 
{
private:

protected:
	Vector3 position;
	Vector3 lookAt;
	Vector3 up;

public:
	
	void setPosition(const Vector3 &newPosition) { position = newPosition; };
	Vector3 getPosition() const { return position; };

	void setUp(const Vector3 &newPosition) { up = newPosition; };
	Vector3 getUp() const { return up; };

	void setLookAt(const Vector3 &newPosition) { lookAt = newPosition; };
	Vector3 getLookAt() const { return lookAt; };

	virtual void Update() = 0;
};


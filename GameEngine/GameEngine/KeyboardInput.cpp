#include "KeyboardInput.h"


KeyboardInput::KeyboardInput()
{
	Handler = new InputMessageHandler();
}


KeyboardInput::~KeyboardInput()
{

}

std::string KeyboardInput::sendMsg()
{
	SimpleMessage* Message;

	if (IS_KEY_DOWN(VK_ESCAPE))
	{
		Message = new SimpleMessage("input", "Escape");
	}

	else if (IS_KEY_DOWN(VK_LEFT))
	{
		Message = new SimpleMessage("input", "Left");
	}

	else if (IS_KEY_DOWN(VK_RIGHT))
	{
		Message = new SimpleMessage("input", "Right");
	}

	else if (IS_KEY_DOWN(VK_UP))
	{
		Message = new SimpleMessage("input", "Up");
	}

	else if (IS_KEY_DOWN(VK_DOWN))
	{
		Message = new SimpleMessage("input", "Down");
	}

	else if (IS_KEY_DOWN(VK_SPACE))
	{
		Message = new SimpleMessage("input", "Jump");
	}
	else if (IS_KEY_DOWN(0x57))
	{
		Message = new SimpleMessage("input", "W");
	}
	else if (IS_KEY_DOWN(0x41))
	{
		Message = new SimpleMessage("input", "A");
	}
	else if (IS_KEY_DOWN(0x53))
	{
		Message = new SimpleMessage("input", "S");
	}
	else if (IS_KEY_DOWN(0x44))
	{
		Message = new SimpleMessage("input", "D");
	}
	else
	{
		Message = nullptr;
	}
	if (Message != nullptr)
	{
		return Message->handle(Handler);
	}
	return "No Input";
}

std::string KeyboardInput::readInput()
{
	return sendMsg();

}

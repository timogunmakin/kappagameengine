#include <string>

class IMesh
{
private:
	typedef struct
	{
		float x;
		float y;
		float z;
	} circle;
	typedef struct
	{
		float a;
		float b;
		float c;
		float d;
	} polygon;

	std::string fileName;
	std::string texture;
	circle * vertices;
	polygon * indices;
	int NUM_VERTS;
	int NUM_POLY;
protected:

public:
//	virtual IMesh getMesh() {  };
	//virtual void setMesh() = 0;

};
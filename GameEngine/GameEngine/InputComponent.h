#pragma once
#include "IComponent.h"
#include "IUpdateable.h"
#include "InputInterface.h"
#include "KeyboardInput.h"

class InputComponent : public IComponent, public IUpdateable
{
private:
	
	InputInterface *inputReader;
	

public:
	InputComponent();
	~InputComponent();

	bool initialize() override ;
	
	void update() override {};
	std::string checkInput();
	void stop();
	
};



#ifndef IMESSAGE_H
#define IMESSAGE_H

#include <string>
#include "IMessageHandler.h"

class IMessage
{
private:
	std::string Message;
	std::string Entity;
	int x;
	int y;
public:
	virtual std::string handle(IMessageHandler* MessageHandler) = 0;
	virtual std::string getMessage() = 0;
};
#endif


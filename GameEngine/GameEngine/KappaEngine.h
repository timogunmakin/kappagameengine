#pragma once

#include "RendererComponent.h"
#include "InputComponent.h"
#include "PlayerComponent.h"
#include "IProperty.h"
#include "SoundManager.h"
#include "Time.h"
#include <iostream>
#include <vector>

class KappaEngine
{
private:
	InputComponent *input;
	RendererComponent *graphics;
	PlayerComponent *players;
	SoundManager* audio;
	Time* time;
	
protected:


public:
	ICamera* Camera;

	KappaEngine();
	~KappaEngine();	
	void addCamera(ICamera* cam);
	void setUp();
	void moveObject(std::string objectName, Vector3 newPosition);
	std::string checkInput();
	void playAudio(std::string songFile);
	void setRendering(IGraphics* renderChoice, int argc, char**argv);
	bool ComponentNumber(int check);
	Vector3 snapCamera(const std::string target);
	void addObject(std::string objectName, std::string objectModelFile, std::string objectTextureFile);
	void setPlayer(std::string playerTarget);
	GameObject* retrieveObject(std::string name);
	
	void addComponentToPlayer(std::string playerNum, IComponent& newComponent);
	void addPropertyToComponent(std::string objectName, IComponent* targetComponent, IProperty* newProperty);
	void run();
};


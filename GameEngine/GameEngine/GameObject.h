#pragma once
#include <vector>
#include <string>
//#include "IComponent.h"
#include "TransformComponent.h"
#include "PhysicsComponent.h"
#include "MeshLoader.h"
#include "IGraphics.h"
#include "staticMesh.h"
#include "VectorData.h"
#include <iostream>

class GameObject 
{
private:
	std::vector<GameObject> children;
	std::vector<IComponent*> components;
	std::string name;
	int playerNum;
	bool isPlayer;
	bool drawAble;

protected:
	TransformComponent transform;

public:
	MeshLoader *mesh; 

	GameObject(const std::string &newName, const std::string &meshPath, const std::string &texturePath);
	GameObject();
	//Copy Constructor
	GameObject(const GameObject &other);
	GameObject &operator=(const GameObject &other);
	//Destructor
	~GameObject();

	//void addComponent(IComponent &newComponent);
	void update();
	Vector3 getPosition();
	void setPosition(Vector3 newPosition);
	Vector3 getScale();
	void setScale(Vector3 newScale);
	const std::string& getName() const;
	void setPlayer();
	void setNPC();
	bool getIsPlayer();
	bool getIsNPC();
	void addComponent(IComponent& newComponent);
	int getPlayerNum();
	int getNPCNum();
	IComponent* getComponent(IComponent* targetComponent);
	void assignComponentUnderComponent(IComponent* newComponent, IComponent* targetComponent);
};


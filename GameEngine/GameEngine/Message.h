#include <string>

class Message
{
	Message(const std::string& entity, const std::string& message);
	Message(const std::string& entity, int x, int y);
	virtual void handle() = 0;

};



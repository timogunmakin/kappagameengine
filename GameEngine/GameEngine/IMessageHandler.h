#pragma once
#include <string>
class IMessage;
class IMessageHandler
{
public:
	virtual std::string process(IMessage* message) = 0;

};

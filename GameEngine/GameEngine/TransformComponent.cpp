#include "TransformComponent.h"
#include "VectorData.h"
#include "Time.h"

TransformComponent::TransformComponent()
{
	
}


TransformComponent::~TransformComponent()
{
}

bool TransformComponent::initialize()
{
	return false;
}

void TransformComponent::update()
{
	if (physicsComponent != nullptr)
	{
		physicsComponent->update(position, time.getTime());
	}	
}

void TransformComponent::assignComponent(IComponent& newComponent)
{
	physicsComponent = &newComponent;
}

Vector3 TransformComponent::getPosition()
{
	return position;
}

void TransformComponent::setPosition(const Vector3& newPosition)
{
	position = newPosition;
}

Vector3 TransformComponent::getScale()
{
	return scale;
}
void TransformComponent::setScale(const Vector3& newScale)
{
	scale = newScale;
}
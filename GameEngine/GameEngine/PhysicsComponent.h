#pragma once
#include "IComponent.h"
#include "IPhysics.h"
#include "Acceleration.h"
#include <vector>
class PhysicsComponent : public IComponent
{
private:
	
	Vector3		displacement;
	Vector3		velocity;
	Vector3		acceleration;
	float		mass;
	Vector3		netForce;
	Vector3		friction;
	float		drag;
	float Laminar;

public:
	PhysicsComponent();
	~PhysicsComponent();
	bool initialize() override;
	void setMass(float mass);
	float getMass();
	float getDragCoef();
	void moveConstAccel(Vector3* position, float timeStep);
	
	void updateNetForce();
	void updateAcceleration();
	void dragLamFlow();
	void dragTurbFlow();
	void dragForce();
	void update(Vector3* position, float timeStep);
	void update() override;
	
	

};


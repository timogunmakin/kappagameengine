#pragma once
#include "InputInterface.h"
#include "SimpleMessage.h"
#include "InputMessageHandler.h"
#include <Windows.h>
#include <stdio.h>
#include <iostream>
#include <string>

#define IS_KEY_DOWN(virt_key_code)  ( (GetAsyncKeyState(virt_key_code) & 0x8000) ? 1 : 0 )
#define IS_KEY_UP(virt_key_code)  ( (GetAsyncKeyState(virt_key_code) & 0x8000) ? 0 : 1 )

class KeyboardInput : public InputInterface
{
private:
	enum EVENT{QUIT, UP, DOWN, LEFT, RIGHT, JUMP} keyEvent;
	HANDLE hInput;
	DWORD NumInputs;
	DWORD InputsRead;
	bool running;
	INPUT_RECORD irInput;
	InputMessageHandler* Handler;

public:
	KeyboardInput();
	~KeyboardInput();
	std::string sendMsg();

	std::string readInput() override;
};


#include "InputComponent.h"



InputComponent::InputComponent()
{
	inputReader = new KeyboardInput();

}

InputComponent::~InputComponent()
{

}

bool InputComponent::initialize()
{
	return false;
}

std::string InputComponent::checkInput()
{
	return inputReader->readInput();
}

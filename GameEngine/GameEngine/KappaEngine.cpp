#include "KappaEngine.h"



KappaEngine::KappaEngine()
{
	
	
}

void KappaEngine::setUp()
{
	if (!graphics->initialize())
	{
		std::cout << "could not start game loop!" << std::endl;

	}
}

void KappaEngine::setRendering(IGraphics* renderChoice, int argc, char**argv)
{
	/*Sets up our rendering library to OpenGL*/
	graphics = new RendererComponent();
	graphics->SetRenderLib(renderChoice, argc, argv);
	input = new InputComponent();
	
	
}

void KappaEngine::moveObject(std::string objectName, Vector3 newPosition)
{
	graphics->getGameObject(objectName)->setPosition(newPosition);
}


KappaEngine::~KappaEngine()
{
}

Vector3 KappaEngine::snapCamera(const std::string target)
{
	return graphics->getGameObject(target)->getPosition();
}


bool KappaEngine::ComponentNumber(int num)
{
	for (int i = 0; i < 100; i++)
	{
		if (num == i)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

void KappaEngine::addObject(std::string objectName, std::string objectModelFile, std::string objectTextureFile)
{
	graphics->addGameObject(objectName, objectModelFile, objectTextureFile);
}

void KappaEngine::setPlayer(std::string playerTarget)
{
	graphics->getGameObject(playerTarget)->setPlayer();
}

void KappaEngine::addCamera(ICamera* cam)
{
	Camera = cam;
}

std::string KappaEngine::checkInput()
{
	return input->checkInput();
}

void KappaEngine::playAudio(std::string songFile)
{
	audio->playSound(songFile.c_str());
	
	
}
GameObject* KappaEngine::retrieveObject(std::string name)
{
	return graphics->getGameObject(name);
}

void KappaEngine::run()
{
			//time->update();
			graphics->poll(this->Camera);
			input->update();
			graphics->update();
			graphics->draw();
			

}


void KappaEngine::addComponentToPlayer(std::string player, IComponent& newComponent)
{
	players->getPlayer(player)->addComponent(newComponent);
}

void KappaEngine::addPropertyToComponent(std::string objectName, IComponent* targetComponent, IProperty* newProperty)
{
	if (graphics->getGameObject(objectName)->getComponent(targetComponent) != nullptr)
	{
		graphics->getGameObject(objectName)->getComponent(targetComponent)->addProperty(newProperty);
	}
	
	
}
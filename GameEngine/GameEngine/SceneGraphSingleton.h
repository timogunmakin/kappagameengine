#pragma once
#include <vector>
#include "GameObject.h"
#include "MeshLoader.h"
class SceneGraphSingleton
{
private:
	int temp;
//	bool temp;
	static SceneGraphSingleton *sceneGraph;
	static bool instanceFlag;
	static SceneGraphSingleton *single;
	std::vector<GameObject *> sceneMeshes;

	SceneGraphSingleton()
	{
		
	}

public:

	static SceneGraphSingleton *getInstance();
	~SceneGraphSingleton()
	{
		instanceFlag = false;
	}

	std::vector<IMesh> getRenderMesh();

	MeshLoader* getMesh(int index){ return sceneMeshes.at(index)->mesh; }
	GameObject* getGameObject(std::string playerTarget);
	GameObject* getGameObject(int playerTarget);
	GameObject* getPlayerGameObject(int playerNum);
	void populateSceneGraph(GameObject* newMesh);
	
	int getMeshCount();
	Vector3 getMeshPosition();
};


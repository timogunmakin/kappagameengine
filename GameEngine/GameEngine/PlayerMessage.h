#pragma once
#include "Message.h"

class PlayerMessage : public Message
{
	void handle(Player player1, MovementType mType, const std::string& message);

};
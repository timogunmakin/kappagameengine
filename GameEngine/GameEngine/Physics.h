#pragma once
#include "IComponent.h"
#include "IUpdateable.h"
#include <float.h>
#include <math.h>
#include <cmath>
//	3D structure  
struct	tagPoint3D
{
	float x, y, z;
};

typedef	tagPoint3D	Point3D, *pPoint2D;

class Physics
{
private:
	Point3D 	pos;				
	Point3D     prevPos;
	Point3D		displacement;	
	Point3D		velocity;
	Point3D		acceleration;
	float		mass;
	Point3D		netForce;
	Point3D		friction;
	float		drag;

public:

	Physics();
	~Physics();

	void setPosition(float xCoord, float yCoord, float zCoord);  
	void setX(float x);
	void setY(float y);
	void setZ(float z);
	float getX();
	float getY();
	float getZ();
	Point3D getPosition();							
	Point3D getPrevPosition();
	Point3D getDisplacement();
	Point3D getVelocity();
	void setVelocity(float x, float y);
	Point3D getNetForce();
	void setNetForce(float force, float force2);
	void updateNetForce(float x1, float y1, float z1, float x2, float y2, float z2);
	void updateAcceleration();
	Point3D getAcceleration();
	float getMass();
	float getDragCoef();
	void setMass(float);

	bool collisionDetection(const Point3D& player);


};
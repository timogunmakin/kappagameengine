#pragma once
#include "IComponent.h"
#include "IUpdateable.h"
#include <windows.h>
#include <mmsystem.h>
#include <queue>

using namespace std;

class SoundManager
{
private:
	queue <LPCSTR> soundsQueue;
	

public:

	SoundManager();
	~SoundManager();
	int OpenALSetup();
	void setBackgroundSong(LPCSTR filename);
	void playSound(LPCSTR filename);
	void stopSound();
	void addSongToQueue(LPCSTR filename);
	void playSongFromQueue();
};
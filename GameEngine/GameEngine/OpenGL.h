#pragma once
#define WIN32
#include "IGraphics.h"
#include <vector>
#include "GameObject.h"
#include "SceneGraphSingleton.h"
#include "glew-1.13.0\include\GL\glew.h"
#include "SDL2-2.0.3\include\SDL.h"

typedef struct
{
	float ambient[4];
	float diffuse[4];
	float specular[4];
} lighting;
typedef struct
{
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float shininess;
} material;



class OpenGL : public IGraphics, public IUpdateable
{
private:
	SDL_Window* window;
	SDL_GLContext context;
	SDL_Event event;
	ICamera* cam;

/*	lighting whiteLighting = {
		{ 0.2f, 0.2f, 0.2f, 1.0f },
		{ 0.7f, 0.7f, 0.7f, 1.0f },
		{ 0.5f, 0.5f, 0.5f, 1.0f }
	};*/

//	float light0_pos[4] = { 1.0, 1.0, 1.0, 0.0 } ;
protected:

public:
	OpenGL(int argc, char**argv);
	OpenGL()
	{
		
		//light0_pos = { 1.0, 1.0, 1.0, 0.0 };
	}
	~OpenGL();	

	void setMaterial(material *mat);
	void setLight(lighting * light);
	bool initialize() override;
	void poll(ICamera* camera) override;
	void startDraw() override;
	void draw() override;
	void endDraw() override;
	
	void update() override;
	
	void drawObjects(MeshLoader* mesh);
	void drawPolygon(MeshLoader* mesh, int a, int b, int c);
	


};


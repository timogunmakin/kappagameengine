#pragma once

class IProperty
{
private:
	

public:
	float x;
	float y;
	float z;
	virtual void initialize() = 0;
	virtual void update() = 0;

};
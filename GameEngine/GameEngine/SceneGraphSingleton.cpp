#include "SceneGraphSingleton.h"

bool SceneGraphSingleton::instanceFlag = false;
SceneGraphSingleton* SceneGraphSingleton::single = nullptr;
SceneGraphSingleton* SceneGraphSingleton::getInstance()
{
	if (!instanceFlag)
	{
		single = new SceneGraphSingleton();
		instanceFlag = true;
		
		return single;
	}
	else
	{
		return single;
	}
}

int SceneGraphSingleton::getMeshCount()
{
	
	if (sceneMeshes.size() != NULL)
	{
		return sceneMeshes.size();
	}
	else
	{
		return 0;
	}
}




void SceneGraphSingleton::populateSceneGraph(GameObject* newMesh)
{
	sceneMeshes.push_back(newMesh);
}

GameObject* SceneGraphSingleton::getGameObject(std::string playerTarget)
{
	
	for (int i = 0; i < sceneMeshes.size(); i++)
	{
		if (sceneMeshes[i]->getName() == playerTarget)
		{
			return sceneMeshes[i];
		}
	}
}



GameObject* SceneGraphSingleton::getGameObject(int playerTarget)
{

	for (int i = 0; i < sceneMeshes.size(); i++)
	{
		if (i == playerTarget)
		{
			return sceneMeshes[i];
		}
	}
}



GameObject* SceneGraphSingleton::getPlayerGameObject(int playerNum)
{
	for (int i = 0; i < sceneMeshes.size(); i++)
	{
		if (sceneMeshes[i]->getIsPlayer() == true && sceneMeshes[i]->getPlayerNum() == playerNum)
		{
			return sceneMeshes[i];
		}
	}
}





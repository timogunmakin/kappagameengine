#pragma once
class IFSMStand
{
public:
	virtual void Stand() = 0;
	virtual int setStandDuration(int standDuration) = 0;
};

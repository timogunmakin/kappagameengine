#include "OpenGL.h"
#include "SceneGraphSingleton.h"



OpenGL::OpenGL(int argc, char**argv)
{
	
}

OpenGL::~OpenGL()
{
	delete window;
}

void OpenGL::poll(ICamera* camera)
{
	this->cam = camera;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT )
		{
			std::cout << "closing..." << std::endl;
			SDL_GL_DeleteContext(context);
			SDL_DestroyWindow(window);
			SDL_Quit();
			break;
		}
	}
	
}

void OpenGL::setMaterial(material *mat) 
{
	material redShinyMaterial = {
		{ 0.80, 0.05, 0.05, 1.0 },
		{ 0.80, 0.05, 0.05, 1.0 },
		{ 1.0, 1.0, 1.0, 1.0 },
		100.0
	};

	
	
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat->shininess);
}

void OpenGL::setLight(lighting * light) 
{
	/*
	
	glLightfv(GL_LIGHT0, GL_AMBIENT, light->ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light->diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light->specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light0_pos);*/
}

void OpenGL::update()
{
	
}


bool OpenGL::initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		
	}
	window = SDL_CreateWindow("Kappa Engine",
		50, 50,
		1280,
		720,
		SDL_WINDOW_OPENGL);

	if (window == NULL)
	{
		std::cout << "Failed to create window: %s" << SDL_GetError() << std::endl;
		return false;
	}

	context = SDL_GL_CreateContext(window);
	if (context == NULL)
	{
		std::cout << "Failed to create OpenGL context: %s" << SDL_GetError() << std::endl;
		return false;
	}

	glClearColor(0.0, 0.0, 1.0, 1.0);
	glColor3f(1.0, 1.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	gluPerspective(45.0, 1, 0.5, 10000.0);
	glEnable(GL_TEXTURE_2D);

	//setLight(&whiteLighting);
	

	SDL_GL_SetSwapInterval(1);
	return true;
}



void OpenGL::startDraw()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity(); // reset the matrix
	gluLookAt(/*pos*/this->cam->getPosition().x, this->cam->getPosition().y, this->cam->getPosition().z,
		/*lookat*/this->cam->getLookAt().x, this->cam->getLookAt().y, this->cam->getLookAt().z,
		/*up*/0,1,0);


}

void OpenGL::draw()
{
	SceneGraphSingleton* sceneGraph = SceneGraphSingleton::getInstance();

	for (int i = 0; i < sceneGraph->getMeshCount(); i++)
	{
		glBindTexture(GL_TEXTURE_2D, i);
		glPushMatrix();
		glColor3f(0.0f, 0.0f, 0.0f);
		//glTranslated(0.0f, 0.0f, 0.0f);
		glTranslated(sceneGraph->getGameObject(i)->getPosition().x, sceneGraph->getGameObject(i)->getPosition().y, sceneGraph->getGameObject(i)->getPosition().z);
		glScalef(0.5f, 0.5f, 0.5f);
		drawObjects(sceneGraph->getMesh(i));
		glPopMatrix();
	}
	
}

void OpenGL::drawPolygon(MeshLoader* mesh, int a, int b, int c)
{
	glBegin(GL_TRIANGLES);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3fv(&mesh->getVertices()[a].x /*&vertices[a].x*/);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3fv(&mesh->getVertices()[b].x);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3fv(&mesh->getVertices()[c].x);
	glTexCoord2f(1.0f, 0.0f);

	glEnd();
}
void OpenGL::drawObjects(MeshLoader* mesh)
{
	glEnableClientState(GL_VERTEX_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, mesh->getFaces());
	glNormalPointer(GL_FLOAT, 0, mesh->getNormals());
	glDrawArrays(GL_TRIANGLES, 0, mesh->getTotalConnectedTriangles());
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);





	/*glBindTexture(GL_TEXTURE_2D, mesh->getTextureNum());
	for (int i = 0; i < mesh->getNumPoly(); i++)
	{
		glColor3f(122, 65, 34);
		drawPolygon(mesh, mesh->getIndices()[i].a, mesh->getIndices()[i].b, mesh->getIndices()[i].c);
	}*/
}

void OpenGL::endDraw()
{
	SDL_GL_SwapWindow(window);
	
}


#pragma once
class Time
{
private:
	float frameStartTime;
	float previousTime;
	float timeDifference;

public:
	Time();
	~Time();
	float getTime();
	void update();
};


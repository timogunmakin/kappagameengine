#pragma once
#include "IMessageHandler.h"
#include "SimpleMessage.h"
#include <iostream>

class InputMessageHandler : public IMessageHandler
{
	std::string process(IMessage* message) override;

};

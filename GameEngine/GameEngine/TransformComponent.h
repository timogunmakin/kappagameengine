#pragma once
#include "IComponent.h"
#include "IUpdateable.h"
#include "VectorData.h"
#include "PhysicsComponent.h"
#include "Time.h"

class TransformComponent : public IComponent, public IUpdateable
{
private:
	Vector3 position;
	Vector3 scale;
	IComponent* physicsComponent;
	Time time;
protected:
	

public:
	
	TransformComponent();
	~TransformComponent();
	bool initialize() override;
	
	void update() override;
	void assignComponent(IComponent& newComponent) override;
	Vector3 getPosition();
	void setPosition(const Vector3& newPosition);
	Vector3 getScale();
	void setScale(const Vector3& newScale);
	
};


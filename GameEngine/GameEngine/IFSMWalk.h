#pragma once
class IFSMWalk
{
public:
	virtual void Walk() = 0;
	virtual int setWalkSpeed(int speed) = 0;
};

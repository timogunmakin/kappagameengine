#include "PhysicsComponent.h"



PhysicsComponent::PhysicsComponent()
{
	name = "Physics";
}


PhysicsComponent::~PhysicsComponent()
{
}

bool PhysicsComponent::initialize()
{
	//set up wether our gameobject has accel / gravity / mass#
	return false;
}


void PhysicsComponent::setMass(float m)
{
	mass = m;
}
float PhysicsComponent::getMass()
{
	return mass;
}


float PhysicsComponent::getDragCoef()
{
	return drag;
}

void PhysicsComponent::moveConstAccel(Vector3* pos, float timeStep)
{
	
	pos->x += velocity.x * (timeStep + 0.5f) * (acceleration.x * (timeStep * timeStep));
	pos->y += velocity.y * (timeStep + 0.5f) * (acceleration.y * (timeStep * timeStep));

	velocity.x += acceleration.x * timeStep;
	velocity.y += acceleration.y * timeStep;
}

void PhysicsComponent::updateNetForce()
{
	// calculate net external force
	for (int i = 0; i < propertyList.size(); i++)
	{
		netForce.x += propertyList[i]->x;
		netForce.y += propertyList[i]->y;
		netForce.y += propertyList[i]->z;
	}

}

void PhysicsComponent::updateAcceleration()
{
	acceleration.x = netForce.x / mass;
	acceleration.y = netForce.y / mass;
	acceleration.z = netForce.z / mass;
}

void PhysicsComponent::dragLamFlow()
{
	
	//dForce.x = -dragFactor * velocity.x;
	//dForce.y = -dragFactor * velocity.y;
}

void PhysicsComponent::dragTurbFlow()
{
	
}

void PhysicsComponent::dragForce()
{
	if (drag == Laminar)
	{
		dragLamFlow();
	}

	else
	{
		dragTurbFlow();
	}
}


void PhysicsComponent::update(Vector3* position, float timeStep)
{
	
	moveConstAccel(position, timeStep);
	updateNetForce();
	updateAcceleration();
	dragForce();
	



	//update all physics parts
	for (int i = 0; i < propertyList.size(); i++)
	{
		propertyList[i]->update();
	}
	
}

void PhysicsComponent::update()
{

	//update all physics parts
	for (int i = 0; i < propertyList.size(); i++)
	{
		propertyList[i]->update();
	}

}


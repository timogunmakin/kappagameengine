#ifndef INPUTINTERFACE_H
#define INPUTINTERFACE_H
#include <string>
class InputInterface
{

public:

	virtual std::string readInput() = 0;

};
#endif
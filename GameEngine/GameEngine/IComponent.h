#pragma once
#include <string>
#include <vector>
#include "IProperty.h"
#include "VectorData.h"
class IComponent
{
private:
	
protected:
	std::string name;
	std::vector<IProperty*> propertyList;
public:
	virtual bool initialize() = 0;
	virtual void addProperty(IProperty* newProperty) { propertyList.push_back(newProperty); };
	virtual void update() = 0;
	virtual void update(Vector3 position, float timeStep) {};
	virtual std::string getComponentName() { return name; };
	virtual void assignComponent(IComponent& newComponent) { };
	
};

#include <KappaEngine.h>
#include "TopDownCamera.h"
#include "Gravity.h"
#include "myPlayer.h"

KappaEngine *engine = new KappaEngine();
TopDownCamera* camera = new TopDownCamera();
Vector3 targetPosition;

Vector3 position;

//myPlayer player;

void setControls()
{
	if (engine->checkInput() == "Escape")
	{
		targetPosition.x -= 0.1f;
		targetPosition.y -= 0.1f;
		targetPosition.z -= 0.1f;
		std::cout << "Escape Key";
		engine->moveObject("Player", targetPosition);


	}

	if (engine->checkInput() == "Left")
	{
		targetPosition.x -= 0.1f;
		std::cout << "Left Key";
		engine->moveObject("Player", targetPosition);
		std::string song = "song.wav";
		engine->playAudio(song);
	}

	if (engine->checkInput() == "Right")
	{
		targetPosition.x += 0.1f;
		std::cout << "Right Key";
		engine->moveObject("Player", targetPosition);
		std::string song = "song.wav";
		engine->playAudio(song);
	}

	if (engine->checkInput() == "Up")
	{
		targetPosition.z += 0.1f;
		std::cout << "Up Key";
		engine->moveObject("Player", targetPosition);
		std::string song = "song.wav";
		engine->playAudio(song);
	}

	if (engine->checkInput() == "Down")
	{
		targetPosition.z -= 0.1f;
		std::cout << "Down Key";
		engine->moveObject("Player", targetPosition);
		std::string song = "song.wav";
		engine->playAudio(song);
	}
}

void Update()
{

	/*Main game loop*/
	while (2 == 2)
	{
		setControls();
		camera->Update();

		position.x = engine->retrieveObject("Player")->getPosition().x + 0.1f;
		position.y = engine->retrieveObject("Player")->getPosition().y + 20.0f;
		position.z = engine->retrieveObject("Player")->getPosition().z;
		cout << engine->retrieveObject("Player")->getPosition().x << endl;
		camera->setPosition(position);
		camera->setLookAt(engine->retrieveObject("Player")->getPosition());
		engine->run();
	}
}

int main(int argc, char**argv)
{
	position.x = 0.0f;
	position.y = 0.0f;
	position.z = 0.0f;

	OpenGL renderer;
	engine->setRendering(&renderer, argc, argv);

	//Lamps
	for (int i = 0; i < 12; i++)
	{
		const std::string &object = "lamp" + std::to_string(i);
		engine->addObject(object, "Models/Lamp.txt", "Models/SphereTexture.raw");
		Vector3 lampLocation;
		lampLocation.x = -10.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (10.0f - -10.0f)));
		lampLocation.y = 0.0f;
		lampLocation.z = -10.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (10.0f - -10.0f)));
		engine->retrieveObject("lamp" + std::to_string(i))->setPosition(lampLocation);
	}
	




	//Handle floor
	const std::string &object = "Floor";
	engine->addObject(object, "Models/Floor.txt", "Models/FloorTexture.raw");
	Vector3 floorScale;
	floorScale.x = 20.0f;
	floorScale.y = 1.0f;
	floorScale.z = 20.0f;
	engine->retrieveObject(object)->setScale(floorScale);

	//player object with physics
	const std::string &playerName = "Player";
	engine->addObject(playerName, "Models/Sphere.txt", "Models/SphereTexture.raw");
	engine->setPlayer("Player");
	PhysicsComponent* newPhysicsComponent = new PhysicsComponent();
	Gravity* gravity = new Gravity();

	engine->addComponentToPlayer("Player", *newPhysicsComponent);
	engine->addPropertyToComponent("Sphere", newPhysicsComponent, gravity);
	
	//Keyboard keyboard;
	//engine->addInput(Keyboard);
	//Initialise targetPosition values - allows controls to 
	targetPosition.x = 0.0f;
	targetPosition.y = 0.0f;
	targetPosition.z = 0.0f;




	
	engine->addCamera(camera);
	//move engine camera to renderer component



	//engine->addTerrain();
	engine->setUp();
	Update();

	return 0;
}
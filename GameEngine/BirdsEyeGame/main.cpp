#include <KappaEngine.h>
#include "TrackingCamera.h"
#include "Gravity.h"

KappaEngine *engine = new KappaEngine();
Vector3 playerPosition;
Vector3 objectPosition;
Vector3 cameraPosition;
TrackingCamera* camera;
bool isRunning;

void playerControls()
{
	if (engine->checkInput() == "Escape")
	{
		isRunning = false;
		std::cout << "Escape Key";
	}

	if (engine->checkInput() == "Up")
	{
		playerPosition.x += 0.1f;
		std::cout << "Up Key";
		engine->moveObject("Player", playerPosition);
	}

	if (engine->checkInput() == "Down")
	{
		playerPosition.x -= 0.2f;
		std::cout << "Down Key";
		engine->moveObject("Player", playerPosition);
	}

	if (engine->checkInput() == "Right")
	{
		if (engine->retrieveObject("Player")->getPosition().z < 4.5f)
		{
			playerPosition.z += 0.3f;
			std::cout << "Right Key";
			engine->moveObject("Player", playerPosition);
		}
	}

	if (engine->checkInput() == "Jump")
	{
		playerPosition.y += 0.2f;
		std::cout << "Spacebar";
		engine->moveObject("Player", playerPosition);
	}
}

void GameLogic()
{
	objectPosition.x -= 0.3;
	engine->moveObject("Non-Player-Sphere", objectPosition);
	if (engine->retrieveObject("Non-Player-Sphere")->getPosition().x < (engine->retrieveObject("Player")->getPosition().x - 30))
	{
		
		objectPosition.x = 0.0f;
		objectPosition.y = engine->retrieveObject("Non-Player-Sphere")->getPosition().y;
		objectPosition.z = engine->retrieveObject("Non-Player-Sphere")->getPosition().z;
		objectPosition.x = engine->retrieveObject("Player")->getPosition().x + 20;
		engine->moveObject("Non-Player-Sphere", objectPosition);

	}
	if (engine->retrieveObject("Player")->getPosition().z > 0.0f)
	{
		playerPosition.z -= 0.2f;
		engine->moveObject("Player", playerPosition);
	}
}

void Update()
{

	/*Main game loop*/
	while (isRunning)
	{
		playerControls();
		camera->Update();
		cameraPosition.x = engine->retrieveObject("Player")->getPosition().x - 10.0f;
		cameraPosition.y = engine->retrieveObject("Player")->getPosition().y;
		cameraPosition.z = engine->retrieveObject("Player")->getPosition().z + 10.0f;
		camera->setPosition(cameraPosition);
		camera->setLookAt(engine->retrieveObject("Player")->getPosition());

		GameLogic();
		engine->run();
	}

}

int main(int argc, char**argv)
{
	OpenGL renderer;
	engine->setRendering(&renderer, argc, argv);
	isRunning = true;

	//Initialise objectPosition values for objects
	objectPosition.x = 10.0f;
	objectPosition.y = 0.0f;
	objectPosition.z = 3.0f;

	//Object with no physics
	const std::string &object = "Non-Player-Sphere";
	engine->addObject(object, "Models/cooper.txt", "Models/SphereTexture.raw");
	engine->moveObject("Non-Player-Sphere", objectPosition);

	//player object with physics
	const std::string &playerName = "Player";
	engine->addObject(playerName, "Models/Sphere.txt", "Models/SphereTexture.raw");
	engine->setPlayer("Player");

	PhysicsComponent* newPhysicsComponent = new PhysicsComponent();
	Gravity* gravity = new Gravity();
	engine->addComponentToPlayer(playerName, *newPhysicsComponent);
	engine->addPropertyToComponent("Player", newPhysicsComponent, gravity);

	//Set initial position and values for player movement
	playerPosition.x = 0.0f;
	playerPosition.y = 0.0f;
	playerPosition.z = 0.0f;
	engine->moveObject("Player", playerPosition);

	//Load audio
	std::string song = "song.wav";
	engine->playAudio(song);

	

	//move engine camera to renderer component
	camera = new TrackingCamera();
	engine->addCamera(camera);

	engine->setUp();
	Update();

	return 0;
}
#pragma once
#include <KappaEngine.h>
class TrackingCamera : public ICamera
{
private:
	Vector3 cameraPosition;
public:
	TrackingCamera();
	~TrackingCamera();
	void Update() override;

};


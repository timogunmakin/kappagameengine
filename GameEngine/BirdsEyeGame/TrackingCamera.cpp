#include "TrackingCamera.h"


TrackingCamera::TrackingCamera()
{
	cameraPosition.x = 5;
	cameraPosition.y = 0;
	cameraPosition.z = 0;
	setPosition(cameraPosition);

	Vector3 lookAt;
	lookAt.x = 10;
	lookAt.y = 0;
	lookAt.z = 0;

	setLookAt(lookAt);
}


TrackingCamera::~TrackingCamera()
{
}

void TrackingCamera::Update()
{
	setLookAt(cameraPosition);
}
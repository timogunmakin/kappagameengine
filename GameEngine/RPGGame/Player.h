#pragma once
#include <KappaEngine.h>
class Player
{	

public:
	Player();
	Player(float x, float y, float z);
	Vector3 getPosition(){ return playerPosition; };
	~Player();

	Vector3 playerPosition;
};
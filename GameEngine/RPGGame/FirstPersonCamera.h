#pragma once
#include <KappaEngine.h>
#include "Player.h"

class FirstPersonCamera : public ICamera
{
private:
	Vector3 FirstPersonPosition;

public:
	FirstPersonCamera();
	~FirstPersonCamera();
	void Update();

};

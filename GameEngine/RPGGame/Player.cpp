#include "Player.h"

Player::Player()
{
	playerPosition.x = 0.0f;
	playerPosition.y = 0.0f;
	playerPosition.z = 0.0f;
}

Player::Player(float x, float y, float z)
{
	playerPosition.x = x;
	playerPosition.y = y;
	playerPosition.z = z;
}

Player::~Player()
{

}
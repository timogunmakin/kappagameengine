#include <KappaEngine.h>
#include "FirstPersonCamera.h"


KappaEngine *engine = new KappaEngine();
Vector3 targetPosition;
Vector3 lookAt;
Player player;
FirstPersonCamera* camera;
Vector3 gunPos;
Vector3 newPos;

void setControls()
{
	if (engine->checkInput() == "Escape")
	{
		targetPosition.x -= 0.1f;
		targetPosition.y -= 0.1f;
		targetPosition.z -= 0.1f;
		std::cout << "Escape Key";
		engine->moveObject("Non-Player-Sphere", targetPosition);
	}

	if (engine->checkInput() == "Left")
	{
		lookAt = engine->Camera->getLookAt();
		lookAt.x += 0.5f;
		engine->Camera->setLookAt(lookAt);
		std::string song = "C:\\song.wav";
		engine->playAudio(song);
	}

	if (engine->checkInput() == "Right")
	{
		lookAt = engine->Camera->getLookAt();
		lookAt.x -= 0.5f;
		engine->Camera->setLookAt(lookAt);
		std::string song = "C:\\song.wav";
		engine->playAudio(song);
	}

	if (engine->checkInput() == "Up")
	{
		lookAt = engine->Camera->getLookAt();
		lookAt.y += 0.5f;
		engine->Camera->setLookAt(lookAt);
		std::string song = "C:\\song.wav";
		engine->playAudio(song);
	}

	if (engine->checkInput() == "Down")
	{
		lookAt = engine->Camera->getLookAt();
		lookAt.y -= 0.5f;
		engine->Camera->setLookAt(lookAt);
		std::string song = "C:\\song.wav";
		engine->playAudio(song);
	}

	if (engine->checkInput() == "W")
	{
		player.playerPosition.z += 0.5;
		lookAt.z += 0.5f;
		engine->Camera->setLookAt(lookAt);
		lookAt.y =- 0.6;
		engine->retrieveObject("Gun")->setPosition(lookAt);
		lookAt.y =+ 0.6;
	}
	if (engine->checkInput() == "S")
	{
		player.playerPosition.z -= 0.5;
		lookAt.z -= 0.5f;
		engine->Camera->setLookAt(lookAt);
		lookAt.y = -0.6;
		engine->retrieveObject("Gun")->setPosition(lookAt);
		lookAt.y = +0.6;
	}
	if (engine->checkInput() == "A")
	{
		player.playerPosition.x += 0.5;
		lookAt.x += 0.5f;
		engine->Camera->setLookAt(lookAt);
		lookAt.y = -0.6;
		engine->retrieveObject("Gun")->setPosition(lookAt);
		lookAt.y = +0.6;
	}
	if (engine->checkInput() == "D")
	{
		player.playerPosition.x -= 0.5;
		lookAt.x -= 0.5f;
		engine->Camera->setLookAt(lookAt);
		lookAt.y = -0.6;
		engine->retrieveObject("Gun")->setPosition(lookAt);
		lookAt.y = +0.6;
	}
}

void Update()
{

	/*Main game loop*/
	while (2 == 2)
	{
		camera->Update();
		setControls();
		engine->Camera->setPosition(player.getPosition());
		engine->Camera->setLookAt(lookAt);
		engine->run();
	}
}

int main(int argc, char**argv)
{

	camera = new FirstPersonCamera();
	OpenGL renderer;
	engine->setRendering(&renderer, argc, argv);

	//Object with no physics
	const std::string &object = "Gun";
	const std::string &object2 = "SkyScraper1";
	const std::string &object3 = "SkyScraper2";
	const std::string &object4 = "SkyScraper3";
	const std::string &object5 = "SkyScraper4";
	const std::string &object6 = "SkyScraper5";
	const std::string &object7 = "SkyScraper6";
	const std::string &object8 = "Humanoid";
	engine->addObject(object, "Models/Gun.txt", "Models/SphereTexture.raw");
	engine->addObject(object2, "Models/SkyScraper.txt", "Models/SphereTexture.raw");
	engine->addObject(object3, "Models/SkyScraper.txt", "Models/SphereTexture.raw");
	engine->addObject(object8, "Models/Humanoid.txt", "Models/SphereTexture.raw");
	engine->addObject(object4, "Models/SkyScraper.txt", "Models/SphereTexture.raw");
	engine->addObject(object5, "Models/SkyScraper.txt", "Models/SphereTexture.raw");
	engine->addObject(object6, "Models/SkyScraper.txt", "Models/SphereTexture.raw");
	engine->addObject(object7, "Models/SkyScraper.txt", "Models/SphereTexture.raw");
	
	gunPos.x = camera->getPosition().x;
	gunPos.y = camera->getPosition().y;
	gunPos.z = camera->getPosition().z + 0.2f;

	newPos.x = gunPos.x + 5;
	newPos.y = gunPos.y;
	newPos.z = gunPos.z;

	engine->retrieveObject("Gun")->setPosition(gunPos);
	engine->retrieveObject("SkyScraper1")->setPosition(newPos);

	newPos.z += 20;

	engine->retrieveObject("SkyScraper2")->setPosition(newPos);
	newPos.z -= 30;
	newPos.x -= 30;
	engine->retrieveObject("Humanoid")->setPosition(newPos);

	engine->retrieveObject("SkyScraper3")->setPosition(newPos);
	newPos.z += 50;
	newPos.x += 50;
	engine->retrieveObject("SkyScraper4")->setPosition(newPos);
	newPos.z -= 80;
	newPos.x -= 80;
	engine->retrieveObject("SkyScraper5")->setPosition(newPos);
	newPos.z += 130;
	newPos.x += 310;
	engine->retrieveObject("SkyScraper6")->setPosition(newPos);
	newPos.z += 30;
	newPos.x += 30;

	targetPosition.x = 0.0f;
	targetPosition.y = 0.0f;
	targetPosition.z = 0.0f;

	lookAt.x = 0.0f;
	lookAt.y = 0.0f;
	lookAt.z = 5.0f;

		
	engine->addCamera(camera);
	//move engine camera to renderer component



	//engine->addTerrain();
	engine->setUp();
	Update();

	return 0;
}